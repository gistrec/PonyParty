<?php
/* Пони правят миром */
namespace PonyTeam;

use pocketmine\plugin\PluginBase;
use pocketmine\event\Listener;
use pocketmine\Player;

/* Эвент отправления команды */
use pocketmine\command\Command;
use pocketmine\command\CommandSender;

/* Эвент выхода игрока с сервера */
use pocketmine\event\player\PlayerQuitEvent;

class PonyTeam extends PluginBase implements Listener{

    /*
     * $leaderName - ник игрока, который пригласил
     * $playerName - ник игрока, которого пригласили
     */
    public $invite = array(/* $leaderName => $playerName */);
    
    /*
     * $leaderName - ник игрока, лидера группы
     * array of players name - массив с никами игроков
     */
    public $teams = array(/* $leaderName => array of players name*/);
    
    public function onEnable() {
        $this->getServer()->getPluginManager()->registerEvents($this, $this);
        $this->getServer()->getLogger("§dPonyTeam загружен!");
    }
    
    // Состоит ли $player в команде
    public function isInTeam($playerName){
        foreach ($this->teams as $leaderName => $players_Name){
            if ($leaderName == $playerName || in_array($playerName, $players_Name)) return true;
        }
        return false;
    }
    
    // $player лидер команды?
    public function isLeader($playerName){
        if (array_key_exists($playerName, $this->teams)) return true;
        return false;
    }
    
    // Получить лидера команды, в которой состоит $player
    public function getLeader($playerName){
        foreach ($this->teams as $leaderName => $players_Name){
            if ($leaderName == $playerName || array_search($playerName, $players_Name) !== FALSE) return $leaderName;
        }
    }
    
    // Получить всех игроков в этой команде
    public function getPlayers($leaderName){
        if (!$this->isLeader($leaderName)) $leaderName = $this->getLeader($leaderName);
        $array = $this->teams[$leaderName];
        array_push($array, $leaderName);
        return $array;
    }
    
    // В одной ли команде два игрока?
    public function isInOneTeam($playerNameOne, $playerNameTwo){
        if (!$this->isInTeam($playerNameOne) && !$this->isInTeam($playerNameTwo)) return false;
        if ($this->getLeader($playerNameOne) == $this->getLeader($playerNameTwo)) return true;
        else return false;
    }

    // Добавить игрока в команду
    public function addPlayerToTeam($leaderName, $playerName){
        // Есть ли у лидера команда
        if ($this->isLeader($leaderName)){
            array_push($this->teams[$leaderName], $playerName);
            $this->sendTeam($leaderName, "§b$playerName §aприсоединился к команде");
        }else {
            $this->teams[$leaderName] = array();
            array_push($this->teams[$leaderName], $playerName);
            $this->sendTeam($leaderName, "§b$playerName §aприсоединился к команде");
        }
    }   

    // Убрать игрока из группы
    public function removeFromTeam($playerName){
        if (count($this->getPlayers($leaderName = $this->getLeader($playerName))) <= 2) {
            if ($playerName == $leaderName) $this->getServer()->getPlayer($this->getPlayers($leaderName)[0])->sendMessage("§b$leaderName §aокинул команду.\nКоманда расформированна из-за малого количества игроков. §bВы больше не состите в команде");
            else $this->getServer()->getPlayer($leaderName)->sendMessage("§b$playerName §aпокинул команду.\n§aКоманда расформированна из-за малого количества игроков. §bВы больше не состите в команде.");
            unset($this->teams[$leaderName]);
        }else {
            // Если вышел лидер
            if ($playerName == $leaderName) {
                $this->createNewLeader($leaderName);
            }else {
                $this->sendTeam("§b$playerName покинул вашу группу");
                unset($this->teams[$leaderName][array_search($playerName, $this->teams[$leaderName])]);
            }
        }
    }

    public function sendMember($leaderName, $text){
        foreach ($this->teams[$leaderName] as $playerName){
            $this->getServer()->getPlayer($playerName)->sendMessage($text);
        }
    }

    public function sendTeam($leaderName, $text){
        foreach ($this->teams[$leaderName] as $playerName){
            $this->getServer()->getPlayer($playerName)->sendMessage($text);
        }
        $this->getServer()->getPlayer($leaderName)->sendMessage($text);
    }

    public function createNewLeader($leaderName){
        $newLeaderName = $this->teams[$leaderName][mt_rand(0, count($this->teams[$leaderName]) - 1)];
        $this->teams[$newLeaderName] = array();
        foreach ($this->teams[$leaderName] as $playerName) {
            if ($newLeaderName != $playerName) array_push($this->teams[$newLeaderName], $playerName);
        }
        $this->sendTeam($newLeaderName, "§b$newLeaderName §aстал лидером команды");
        unset($this->teams[$leaderName]);
    }

    /*
     * Обрабатываем команды игрока
     */
    public function onCommand(CommandSender $sender, Command $cmd, $label, array $args){
        // Если эта команда не /team, завершаем функцию
        if (!strtolower($cmd->getName()) == 'team') return false;
        // Если нет первого аргумента функции, пишем об этом игроку и прекращаем функцию
        if (!isset($args[0])) return($sender->sendMessage("§aЧтобы узнать информацию, введите §b/team help"));
        switch (strtolower($args[0])) {
            case 'invite':
                // Не введен ник игрока
                if (!isset($args[1])) return($sender->sendMessage("§aИспользуйте §b/team invite <name> §aчтобы пригласить игрока."));
                // Когда приглашаешь сам себя
                if (strtolower($sender->getName()) == strtolower($args[1])) return($sender->sendMessage("§aНельзя приглашать самого себя"));
                // Игрок не лидер группы
                if ($this->isInTeam($sender->getName()) && !$this->isLeader($sender->getName())) return($sender->sendMessage("§aТолько лидер группы может приглашать игроков"));
                // Игрок не сети
                if (!(($player = $this->getServer()->getPlayer($args[1])) instanceof Player) || !$player->isOnline()) return($sender->sendMessage("§aИгрок не в сети!"));
                // Игрок уже в другой команде
                if ($this->isInTeam($player->getName())) return($sender->sendMessage("§aИгрок уже состоит в команде!"));
                // Если приглашение уже отправлено игроку
                if (in_array($sender->getName(), $this->invite) && $this->invite[$sender->getName()] == $player->getName()) return($sender->sendMessage("§aВы уже пригласили этого игрока"));
                // Игрока можно пригласить
                $player->sendMessage("§b".$sender->getName()." §aприглашает вас в команду. Чтобы всупить, введите §b/team join");
                $sender->sendMessage("§aВы отправили игроку §b".$player->getName()." §aприглашение.");
                $this->invite[$sender->getName()] = $player->getName(); //TODO: check, can delete $a =& and write &$sender
                break;
            case 'kick':
                // Если игрок не в команде
                if (!$this->isInTeam($sender->getName())) return($sender->sendMessage("§aВы не состоите в команде!"));
                // Если не введен ник
                if (!isset($args[1])) return($sender->sendMessage("§aИспользуйте §b/team kick <name> §aчтобы выгнать игрока из команды."));
                // Если игроки не в одной группы
                if (!$this->isInOneTeam($sender->getName(), $args[1]));
                // Если игрок не лидер
                if ($sender->getName() != $this->isLeader($sender->getName())) return($sender->sendMessage("§aТолько лидер группы может выгонять игроков"));
                // Кикаем игрока
                $this->removeFromTeam($sender->getName(), $args[1]);
                $this->sendTeam($sender->getName(), "§a".sender->getName()." исключил из группы игрока §b".$args[1]);
            case 'join':
                // Если игрок уже в команде
                if ($this->isInTeam($sender->getName())) return($sender->sendMessage("§aВы уже состоите в команде! Чтобы покинуть, введите §b/team exit"));
                // Если игрока еще не приглашали
                if (($leaderName = array_search($sender->getName(), $this->invite)) === FALSE) return($sender->sendMessage("§aВас еще не пригласили в команду или предложение отозвано."));
                // Присоединяем игрока к команде
                $this->addPlayerToTeam($leaderName, $sender->getName());
                unset ($this->invite[$leaderName]);
                break;
            case 'info':
                // Если игрок в группе
                if (!$this->isInTeam($sender->getName())) return($sender->sendMessage("§aВы не состоите в команде!"));
                // Пишем ему сообщение о группе
                $sender->sendMessage("§aГлава группы: §b".$this->getLeader($sender->getName())."\n§aВ группе: §b".implode(', ', $this->teams[$this->getLeader($sender->getName())]));
                break;
            case 'exit':
                // Если игрок не в тиме - пишем ему об этом
                if (!$this->isInTeam($sender->getName())) return($sender->sendMessage("§aВы не состоите в команде!"));
                $this->removeFromTeam($sender->getName());
                $sender->sendMessage("§aВы покинули команду");
                break;
            case 'help':
                $sender->sendMessage("§aДоступные команды:\n§b/team invite §aпригласить игрока в команду.\n§b/team join §aпринять приглашение\n§b/team info §aинформация о своей команде\n§b/team exit §aвыйти из группы");
                break;
            default:
                $sender->sendMessage("§aИспользуйте §b/team help §aчтобы получить информацию");
                break;
        }
    }
    
    /*
     * TODO: Если игрок лидер - находим нового лидера
     *       Если игрок просто в команде - убираем его
     */
    public function onQuit(PlayerQuitEvent $event) {
        $playerName = $event->getPlayer()->getName();
        if ($this->isInTeam($playerName)) $this->removeFromTeam($playerName);
        if (isset($this->invite[$playerName])) unset($this->invite[$playerName]);
        while (($key = array_search($playerName, $this->invite)) !== FALSE){
            unset($this->invite[$key]);
        }
    }
    
}