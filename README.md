# Плагин на команды для сервера minecraft pe

**[Автор плагина](http://vk.com/AreaOfDefect)**


* С этим плагином игроки смогут приглашать друг-друга в команды.

* Очень простые функции позволят вам легко дописать тот функционал, который нужен именно вам!

* Позже добавлю описание функций, и пример их использования.
***


Команды
====
| Команда             | Описание                           | Доступно |
| :-----------------  |:---------------------------------- | :------  |
| /team invite Name   | Пригласить игрока к себе в команду | Лидеру   |
| /team kick Name     | Выгнать игрока из команды          | Лидеру   |
| /team join          | Присоединиться к команде           | Всем     |
| /team info          | Показать информацию о команде      | Всем     |
| /team exit          | Покинуть команду                   | Всем     |


Описание функций
====
| Функция                     | Входные данные                        | Результат       | Описание                         |
| :-------------------------  | :------------------------------------ | :-------------- | :------------------------------- |
| isInTeam($name)             | Ник игрока                            | true/false      | Состоит ли в команде игрок       |
| isLeader($name)             | Ник игрока, который состоит в команде | true/false      | Игрок лидер команды?             | 
| getLeader($name)            | Ник игрока, который состоит в команде | Ник игрока      | Получить лидера команды          |
| getPlayers($name)           | Ник игрока, который состоит в команде | Массив с никами | Получить всех игроков в команде  |
| isInOneTeam($name1, $name2) | Два ника                              | true/false      | Состоят ли игроки в одно команде | 
| sendTeam($name)             | Ник игрока, который состоит в команде | null            | Отправить сообщение всей команде |


Пример использования API:
======

Отмена pvp между игроками в одной команде
------
    $this->team = $this->getPluginManager()->getPlugin("PonyTeam");
        
    public function onDamage(EntityDamageEvent $event){
        $damager = $event->getDamager;
        $target = $event->getEntity();
        if ($damager instanceof Player && $target instanceof Player){
            if ($this->team->isInOneTeam($damager->getName(), $target->getName())){
                $damager->sendMessage("Вы состоите в одной команде");    
                $event->setCancelled();
            }
        }
    }
 
 
Отправлять сообщение команде
------
    $this->team = $this->getPluginManager()->getPlugin("PonyTeam");
    
    public function onChat(PlayerChatEvent $event){
        $player = $event->getPlayer();
        if ($event->getMessage(){0} == "!"){
            $event->setCancelled();
            if ($this->team->isInTeam($player->getName()){
                foreach ($this->team->getPlayers($player->getName() as $playerName()){
                    $this->getServer()->getPlayer($playerName)->sendMessage($event->getMessage());
                }
            }else {
                $player->sendMessage("Вы не состоите в команде");
            }
        }
    }
    
    
    
    
    
    
    
    
 